NAME
  ncaptool - Network capture library

SYNOPSIS
  ncaptool [-h] [-d] [-m] [-f] [-r] [-w] [-v] [-S] [-e] [-i]
           [-b] [-p] [-n] [-l] [-g] [-o] [-s] [-c] [-t] [-1]
           [-2] [-k] [-Dmod] [-H]

DESCRIPTION
 ncaptool is a network capture library like libpcap (on which it is based)
 and tcpdump. It produces binary data in its own ncap format, which can be
 stored in a dump file or transmitted over a UDP socket. Unlike libpcap, it
 discards data link headers and only supports IPv4 and IPv6 packets, but it
 can perform reassembly of IP datagrams.

OPTIONS
  -h            display this help text and exit
  -d            increment debugging level
  -m            increment message trace level
  -f            flush outputs after every bufferable write
  -r            destination of -s can be a remote (off-LAN) address
  -w            use wallclock time not NCAP timestamp for -o files
  -v            emit a traffic summary to stderr on exit
  -S            stripe across all -s datasinks, round robin style
  -e endline    specify continuation separator
  -i ifname[+]  add interface as a datasource ('+' = promiscuous)
  -b bpf        use this bpf pattern for any -i or -p datasources
  -p file       add pcap file as a datasource ('-' = stdin)
  -n file       add ncap file as a datasource ('-' = stdin)
  -l socket     add datagram socket as a datasource (addr/port)
  -g file       write msg trace to this file ('-' = stdout)
  -o file       write ncap data to this file ('-' = stdout)
  -s so[,r[,f]]  add this datagram socket as a datasink (addr/port)
                (optional ,r is the transmit rate in messages/sec)
                (optional ,f is schedule frequency, default is 100)
  -c count      stop or reopen after this many msgs are processed
  -t interval   stop or reopen after this amount of time has passed
  -1 [+-]value  replace, set (+), or clear (-) user1 to this value
  -2 [+-]value  replace, set (+), or clear (-) user1 to this value
  -k cmd        make -c, -t continuous, run cmd on each new file
                (cmd can be empty if you just want the continuity)
  -Dmod[,args]  add module
  -H [sd]       hide source and/or destination IP addresses

  argument to -l and -s can be addr/port or addr/port..port (range)

EXAMPLE
  Common usage:

    $ ncaptool -t 3600 -k gzip -i enp9s0+ -o $FILE

  to inspect a compressed ncap file, run something like this:

    $ zcat $FILE | ncaptool -n - -vmg -

SEE ALSO
  ncap(3), tcpdump(8).

AUTHOR
  ncaptool was written by Internet Systems Consortium and Jan Andres <jandres@gmx.net>.

  This manual page was written by Thiago Andrade Marques <thmarques@gmail.com> for the Debian project (but may be used by others).
